const txtFirstName = document.querySelector('#txt-first-name');
const txtlastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

const completeName = () => {
	let firstName = txtFirstName.value;
	let lastName = txtlastName.value;
	spanFullName.innerHTML = firstName + ' ' + lastName;
}
txtFirstName.addEventListener('keyup', completeName);
txtlastName.addEventListener('keyup', completeName);

	
